<?php
$page="Pendapatan";
include "koneksi.php";
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title><?= $page; ?> | Sistem Informasi Parkir</title>

  <?php include ('css.php'); ?>
<body class="hold-transition sidebar-mini">
<div class="wrapper">

  <?php include('navbar.php'); ?>

  <?php include('sidebar.php'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Pendapatan</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Parkir</a></li>
              <li class="breadcrumb-item active">Pendapatan</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
       

            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Pendapatan</h3> 
              </div>
              <!-- /.card-header -->
              <div class="card-body">
              <form action="pendapatan.php" method="GET">
                  <div class="row">
                    <div class="col-12">
                    <div class="form-group">
                      <label>TANGGAL </label>
                      <input type="date" class="form-control" name="tanggal" placeholder="MASUKKAN TANGGAL">
                      
                    </div>
                    <div class="form-group">
                      <button type="submit" class="btn btn-primary btn-sm">Cari </button>
                    </div>
                  </div>
                  </div>
                  
                </form>
               <?php 
                if(isset($_GET['tanggal'])){
                  $tanggal = $_GET['tanggal'];
                  echo "<b>Hasil pencarian pendapatan pada : ".$tanggal."</b>";
                }
                ?>
                
                  <br><hr>
                  <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>#</th>
                    <th>Nomor Plat</th>
                    <th>Tarif</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php 
                    error_reporting(0);
                    $no=1;
                    if(isset($_GET['tanggal'])){
                    $tanggal = $_GET['tanggal'];
                    $query = "SELECT * FROM parkir JOIN kendaraan ON parkir.nomor_plat = kendaraan.nomor_plat JOIN jenis_kendaraan ON kendaraan.kode_jeniskendaraan = jenis_kendaraan.kode_jeniskendaraan WHERE tanggal_masuk = '$tanggal' ";
                    $result = mysqli_query($koneksi, $query);
                    }else {

                    }
                    
                    while ($data = mysqli_fetch_assoc($result)) {
                      $pendapatan += $data['tarif'];
                    ?>
                      <tr>
                        <td><?= $no++; ?></td>
                        <td><?= $data['nomor_plat']; ?></td>
                        <td><?= $data['tarif']; ?></td>
                      </tr>
                      <?php } ?>
                  </tbody>
                  </table>
                  <br><hr>
                  <div class="alert alert-info text-center" role="alert">
                    Total Pendapatan pada tanggal <?= $tanggal; ?> adalah <?= number_format($pendapatan); ?>
                  </div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
  <?php include ('footer.php'); ?>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<?php include ('js.php'); ?>
</body>
</html>
