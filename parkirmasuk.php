<?php
$page = "Parkir Masuk";
include "koneksi.php";
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title><?= $page; ?> | Sistem Informasi Parkir</title>

  <?php include ('css.php'); ?>
<body class="hold-transition sidebar-mini">
<div class="wrapper">

  <?php include('navbar.php'); ?>

  <?php include('sidebar.php'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Parkir Masuk</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Parkir</a></li>
              <li class="breadcrumb-item active">Parkir Masuk</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
       

            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Parkir Masuk</h3> 
              
               
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <form action="parkirmasuk.php" method="GET">
                  <div class="row">
                    <div class="col-12">
                    <div class="form-group">
                      <label>NOMOR PLAT</label>
                      <input type="text" class="form-control" name="nomor_plat" placeholder="MASUKKAN PLAT NOMOR">
                      
                    </div>
                    <div class="form-group">
                      <button type="submit" class="btn btn-primary btn-sm">Cari </button>
                    </div>
                  </div>
                  </div>
                  
                </form>
               <?php 
                if(isset($_GET['nomor_plat'])){
                  $nomor_plat = $_GET['nomor_plat'];
                  echo "<b>Hasil pencarian : ".$nomor_plat."</b>";
                }
                ?>
                <?php
                error_reporting(0); 
                  if(isset($_GET['nomor_plat'])){
                    $nomor_plat = $_GET['nomor_plat'];
                    if(empty($nomor_plat)){
                      echo "Pencarian Kosong";
                    }else{
                      $data = mysqli_query($koneksi,"select * from kendaraan where nomor_plat like '%".$nomor_plat."%'");   
                    $d = mysqli_fetch_assoc($data);
                    }
                  }else if(isset($_GET['nomor_plat'])==""){

                  }else {
                    echo "salah"; 
                  }
                  $no = 1;
                  ?>
                     <form action="input_masukparkir.php" method="POST">
                       <div class="form-group">
                         <label>Nomor plat</label>
                         <input type="text" name="nomor_plat" class="form-control" value="<?= $d['nomor_plat']; ?>" readonly>
                       </div>
                       <div class="form-group">
                         <label>Tanggal Masuk</label>
                         <input type="date" class="form-control" name="tanggal_masuk" value="<?= date('Y-m-d'); ?>" readonly required>
                       </div>
                       <div class="form-group">
                         <label>Jam Masuk</label>
                         <input type="time" class="form-control" name="jam_masuk" value="<?= date("H:i:s") ?>" required readonly>
                       </div>
                       <div class="form-group">
                         <button type="submit" class="btn btn-success float-right">Submit</button>
                       </div>
                     </form>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Data <?= $page ?></h3> 
              
               
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                
              <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>#</th>
                    <th>Nomor Plat</th>
                    <th>Jam Masuk</th>
                    <th>Tanggal Masuk</th>
                    <th width="7%">Action</th>
                  </tr>
                  </thead>
                  <tbody>
                    <?php 
                    error_reporting(0);
                    $query = "SELECT * FROM parkir JOIN kendaraan ON parkir.nomor_plat = kendaraan.nomor_plat JOIN jenis_kendaraan ON kendaraan.kode_jeniskendaraan = jenis_kendaraan.kode_jeniskendaraan";
                    $result = mysqli_query($koneksi, $query);
                    $no=1;
                    while ($data = mysqli_fetch_assoc($result)) {
                      $pendapatan += $data['tarif'];
                    ?>
                  <tr>
                    <td><?= $no++; ?></td>
                    <td><?= $data['nomor_plat']; ?></td>
                    <td><?= $data['jam_masuk']; ?></td>
                    <td><?= $data['tanggal_masuk']; ?></td>
                    
                    <td align='center'>
                    <button class="btn btn-success btn-xs" data-toggle="modal" data-target="#inputkeluar<?= $no; ?>"><i class="fa fa-edit"></i></button> 
                   
                  </td>
                  </tr>
                  
                                <!-- Modal -->
              <div class="modal fade" id="inputkeluar<?= $no; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Update Parkir Keluar</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <form action="input_keluarparkir.php" method="post">
                        <div class="form-group">
                          <label for="nomor_plat">Nomor Plat</label>
                          <input type="hidden" name="nomor_parkir" value="<?= $data['nomor_parkir']; ?>">
                          <input type="text" name="nomor_plat" class="form-control" value="<?= $data['nomor_plat']; ?>" readonly>
                        </div>
                        <div class="form-group">
                          <label for="tanggal_keluar">Tanggal Keluar</label>
                          <input type="date" name="tanggal_keluar" class="form-control" value="<?= date('Y-m-d'); ?>" readonly>
                        </div>
                        <div class="form-group">
                          <label for="jam_keluar">Jam Keluar</label>
                          
                          <input type="time" name="jam_keluar" class="form-control" value="<?= date("H:i:s") ?>" readonly>
                        </div>
                      <div class="form-group">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      <button type="submit" class="btn btn-primary float-right">Save changes</button>
                      </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>

                  <?php } ?>

                  </tbody>
                  <tfoot>
                  <tr>
                  <th>#</th>
                    <th>Nomor Plat</th>
                    <th>Jam Masuk</th>
                    <th>Tanggal Masuk</th>
                    <th width="7%">Action</th>
                  </tr>
                  </tfoot>
                </table>
              
                
                     
              </div>
              <!-- /.card-body -->
            </div>
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
  <?php include ('footer.php'); ?>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<?php include ('js.php'); ?>
</body>
</html>
