-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 01 Jan 2023 pada 18.21
-- Versi server: 10.4.6-MariaDB
-- Versi PHP: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_uas`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `deposit`
--

CREATE TABLE `deposit` (
  `kode_deposit` int(11) NOT NULL,
  `npm` varchar(20) NOT NULL,
  `jam_tanggal` datetime NOT NULL,
  `nominal` int(11) NOT NULL,
  `bukti_transfer` varchar(255) NOT NULL,
  `status_pembayaran` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `jenis_kendaraan`
--

CREATE TABLE `jenis_kendaraan` (
  `kode_jeniskendaraan` varchar(20) NOT NULL,
  `jenis_kendaraan` varchar(50) NOT NULL,
  `tarif` int(11) NOT NULL,
  `keterangan` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jenis_kendaraan`
--

INSERT INTO `jenis_kendaraan` (`kode_jeniskendaraan`, `jenis_kendaraan`, `tarif`, `keterangan`) VALUES
('MBL001', 'Mobil', 5000, 'Pegawai/Dosen\r\n'),
('MBL002', 'Mobil', 3000, 'Mahasiswa'),
('SPD001', 'Motor', 2000, 'Pegawai/Dosen'),
('SPD002', 'Motor', 1000, 'Mahasiswa');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jurusan`
--

CREATE TABLE `jurusan` (
  `kode_jurusan` varchar(20) NOT NULL,
  `nama_jurusan` varchar(100) NOT NULL,
  `keterangan` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jurusan`
--

INSERT INTO `jurusan` (`kode_jurusan`, `nama_jurusan`, `keterangan`) VALUES
('FST001', 'Sistem Informasi\r\n', ''),
('FST002', 'Teknik Informatika', ''),
('FST003', 'Matematika', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kendaraan`
--

CREATE TABLE `kendaraan` (
  `nomor_plat` varchar(20) NOT NULL,
  `nomor_stnk` varchar(20) NOT NULL,
  `kode_jeniskendaraan` varchar(20) NOT NULL,
  `npm` varchar(20) DEFAULT NULL,
  `id_pegawai` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kendaraan`
--

INSERT INTO `kendaraan` (`nomor_plat`, `nomor_stnk`, `kode_jeniskendaraan`, `npm`, `id_pegawai`) VALUES
('AG 1509 BI', '84728429', 'SPD001', NULL, 'SPK1'),
('AG 5482 GY', '84938274', 'SPD002', '20040302002', NULL),
('L 9538 VC', '52945823', 'MBL001', NULL, 'DOSENFST2'),
('N 1900 CC', '38592841', 'MBL002', '200403020011', NULL),
('N 3275 BG', '41869283', 'SPD001', NULL, 'SPK2'),
('N 7284 VG', '48592382', 'MBL002', '200403020021', NULL),
('N 8392 HR', '84928490', 'MBL002', '20040302005', NULL),
('N 8493 FG', '84940294', 'MBL001', NULL, 'DOSENFST3'),
('N 8903 DC', '22113344', 'SPD002', '200403020013', NULL),
('W 2100 AS', '49185932', 'MBL001', NULL, 'DOSENFST1');

-- --------------------------------------------------------

--
-- Struktur dari tabel `mahasiswa`
--

CREATE TABLE `mahasiswa` (
  `npm` varchar(20) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `jenis_kelamin` varchar(20) NOT NULL,
  `alamat` text NOT NULL,
  `telepon` varchar(15) NOT NULL,
  `foto` varchar(255) NOT NULL,
  `kode_jurusan` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `mahasiswa`
--

INSERT INTO `mahasiswa` (`npm`, `nama`, `password`, `jenis_kelamin`, `alamat`, `telepon`, `foto`, `kode_jurusan`) VALUES
('200403020011', 'Natasha Maulidha', 'd41d8cd98f00b204e9800998ecf8427e', 'Perempuan', 'Jl. Klayatan', '081284930643', '', 'FST001'),
('200403020013', 'Elvina Rosanti', '827ccb0eea8a706c4c34a16891f84e7b', 'Perempuan', 'Binangun Baru Pakisaji Malang', '081331753326', '', 'FST001'),
('20040302002', 'Dani Kurniawan', 'd41d8cd98f00b204e9800998ecf8427e', 'Laki - Laki', 'Jl. Kedungkandang', '08949385723', '', 'FST002'),
('200403020021', 'Bestario', 'd41d8cd98f00b204e9800998ecf8427e', 'Laki - Laki', 'Jl. Sukun', '08295835921', '', 'FST002'),
('20040302005', 'Salman Audi', 'd41d8cd98f00b204e9800998ecf8427e', 'Laki -Laki', 'Jl. Sebrang ', '089748392842', '', 'FST003');

-- --------------------------------------------------------

--
-- Struktur dari tabel `parkir`
--

CREATE TABLE `parkir` (
  `nomor_parkir` int(11) NOT NULL,
  `jam_masuk` time NOT NULL DEFAULT current_timestamp(),
  `tanggal_masuk` date NOT NULL ,
  `jam_keluar` time DEFAULT NULL,
  `tanggal_keluar` date DEFAULT NULL,
  `nomor_plat` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `parkir`
--

INSERT INTO `parkir` (`nomor_parkir`, `jam_masuk`, `tanggal_masuk`, `jam_keluar`, `tanggal_keluar`, `nomor_plat`) VALUES
(18, '17:58:34', '2022-12-30', NULL, NULL, 'AG 1509 BI'),
(19, '17:59:25', '2022-12-30', '19:01:05', '2022-12-30', 'AG 5482 GY'),
(20, '20:00:37', '2022-12-30', NULL, NULL, 'L 9538 VC');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pegawai`
--

CREATE TABLE `pegawai` (
  `id_pegawai` varchar(20) NOT NULL,
  `nama_pegawai` varchar(100) NOT NULL,
  `foto` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `jenis_kelamin` varchar(20) NOT NULL,
  `alamat` text NOT NULL,
  `telepon` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pegawai`
--

INSERT INTO `pegawai` (`id_pegawai`, `nama_pegawai`, `foto`, `password`, `jenis_kelamin`, `alamat`, `telepon`) VALUES
('DOSENFST1', 'Syaiful Anwar', '', '827ccb0eea8a706c4c34a16891f84e7b', 'Laki - Laki', 'Jl. Karya Lor', '081234568392'),
('DOSENFST2', 'Sinta Ananis', '', 'd41d8cd98f00b204e9800998ecf8427e', 'Perempuan', 'Jl. Kartanegara', '081475982940'),
('DOSENFST3', 'Dwi Maharani\r\n', '', 'd41d8cd98f00b204e9800998ecf8427e', 'Perempuan', 'Jl. Mawar', '08968472402'),
('SPK1', 'Karyono\r\n', '', 'd41d8cd98f00b204e9800998ecf8427e', 'Laki - Laki', 'Jl. Ijen Baru', '083185938593'),
('SPK2', 'Jamaludin', '', 'd41d8cd98f00b204e9800998ecf8427e', 'Laki - Laki', 'Jl. Tenggumung', '089658493021');

-- --------------------------------------------------------

--
-- Struktur dari tabel `petugas`
--

CREATE TABLE `petugas` (
  `id_petugas` varchar(20) NOT NULL,
  `nama_petugas` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `jenis_kelamin` varchar(20) NOT NULL,
  `alamat` text NOT NULL,
  `telepon` varchar(15) NOT NULL,
  `foto` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `deposit`
--
ALTER TABLE `deposit`
  ADD PRIMARY KEY (`kode_deposit`),
  ADD KEY `npm` (`npm`);

--
-- Indeks untuk tabel `jenis_kendaraan`
--
ALTER TABLE `jenis_kendaraan`
  ADD PRIMARY KEY (`kode_jeniskendaraan`);

--
-- Indeks untuk tabel `jurusan`
--
ALTER TABLE `jurusan`
  ADD PRIMARY KEY (`kode_jurusan`);

--
-- Indeks untuk tabel `kendaraan`
--
ALTER TABLE `kendaraan`
  ADD PRIMARY KEY (`nomor_plat`),
  ADD KEY `kode_jeniskendaraan` (`kode_jeniskendaraan`),
  ADD KEY `npm` (`npm`),
  ADD KEY `id_pegawai` (`id_pegawai`);

--
-- Indeks untuk tabel `mahasiswa`
--
ALTER TABLE `mahasiswa`
  ADD PRIMARY KEY (`npm`),
  ADD KEY `kode_jurusan` (`kode_jurusan`);

--
-- Indeks untuk tabel `parkir`
--
ALTER TABLE `parkir`
  ADD PRIMARY KEY (`nomor_parkir`),
  ADD KEY `nomor_plat` (`nomor_plat`);

--
-- Indeks untuk tabel `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`id_pegawai`);

--
-- Indeks untuk tabel `petugas`
--
ALTER TABLE `petugas`
  ADD PRIMARY KEY (`id_petugas`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `deposit`
--
ALTER TABLE `deposit`
  MODIFY `kode_deposit` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `parkir`
--
ALTER TABLE `parkir`
  MODIFY `nomor_parkir` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `deposit`
--
ALTER TABLE `deposit`
  ADD CONSTRAINT `deposit_ibfk_1` FOREIGN KEY (`npm`) REFERENCES `mahasiswa` (`npm`) ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `kendaraan`
--
ALTER TABLE `kendaraan`
  ADD CONSTRAINT `kendaraan_ibfk_1` FOREIGN KEY (`kode_jeniskendaraan`) REFERENCES `jenis_kendaraan` (`kode_jeniskendaraan`) ON UPDATE CASCADE,
  ADD CONSTRAINT `kendaraan_ibfk_2` FOREIGN KEY (`npm`) REFERENCES `mahasiswa` (`npm`) ON UPDATE CASCADE,
  ADD CONSTRAINT `kendaraan_ibfk_3` FOREIGN KEY (`id_pegawai`) REFERENCES `pegawai` (`id_pegawai`) ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `mahasiswa`
--
ALTER TABLE `mahasiswa`
  ADD CONSTRAINT `mahasiswa_ibfk_1` FOREIGN KEY (`kode_jurusan`) REFERENCES `jurusan` (`kode_jurusan`) ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `parkir`
--
ALTER TABLE `parkir`
  ADD CONSTRAINT `parkir_ibfk_1` FOREIGN KEY (`nomor_plat`) REFERENCES `kendaraan` (`nomor_plat`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
