<?php
// memanggil library FPDF
require('fpdf/fpdf.php');
include 'koneksi.php';
 
// intance object dan memberikan pengaturan halaman PDF
$pdf=new FPDF('P','mm','A4');
$pdf->AddPage();
 
$pdf->SetFont('Times','B',13);
$pdf->Cell(200,10,'DATA JENIS KENDARAAN',0,0,'C');
 
$pdf->Cell(10,15,'',0,1);
$pdf->SetFont('Times','B',9);
$pdf->Cell(10,7,'NO',1,0,'C');
$pdf->Cell(40,7,'Kode Jenis Kendaraan' ,1,0,'C');
$pdf->Cell(50,7,'Jenis Kendaraan',1,0,'C');
$pdf->Cell(20,7,'Tarif',1,0,'C');
$pdf->Cell(75,7,'Keterangan',1,0,'C');
 
 
$pdf->Cell(10,7,'',0,1);
$pdf->SetFont('Times','',10);
$no=1;
$data = mysqli_query($koneksi,"SELECT  * FROM jenis_kendaraan");
while($d = mysqli_fetch_array($data)){
  $pdf->Cell(10,6, $no++,1,0,'C');
  $pdf->Cell(40,6, $d['kode_jeniskendaraan'],1,0,'C');
  $pdf->Cell(50,6, $d['jenis_kendaraan'],1,0,'C');  
  $pdf->Cell(20,6, $d['tarif'],1,0,'C');
  $pdf->Cell(75,6, $d['keterangan'],1,1,'C');
}
 
$pdf->Output();
 
?>