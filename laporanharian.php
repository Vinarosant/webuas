<?php
$page="Laporan";
include "koneksi.php";
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title><?= $page ?> | Sistem Informasi Parkir</title>

  <?php include ('css.php'); ?>
<body class="hold-transition sidebar-mini">
<div class="wrapper">

  <?php include('navbar.php'); ?>

  <?php include('sidebar.php'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Laporan Harian</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Parkir</a></li>
              <li class="breadcrumb-item active">Laporan Harian</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
       

            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Laporan Harian</h3> 
              </div>
              <!-- /.card-header -->
              <div class="card-body">
              <form action="laporanharian.php" method="GET">
                  <div class="row">
                    <div class="col-6">
                    <div class="form-group">
                      <label>Tanggal Laporan</label>
                      <input type="date" class="form-control" name="tanggal" placeholder="MASUKKAN TANGGAL">
                      
                    </div>
                    <div class="form-group">
                      <button type="submit" class="btn btn-primary btn-sm">Cari </button>
                    </div>
                  </div>
                  </div>
                  
                </form>
               <?php 
                if(isset($_GET['tanggal'])){
                  $tanggal = $_GET['tanggal'];
                  echo "<b>Hasil pencarian : ".$tanggal."</b>";
                }
                ?>
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>#</th>
                    <th>Nomor Plat</th>
                    <th>Jam Masuk</th> 
                    <th>Tanggal Masuk</th>
                    <th>Jam Keluar</th>
                    <th>Tanggal Keluar</th>
                    <th>Tarif</th>
                    <th width="7%">Action</th>
                  </tr>
                  </thead>
                  <tbody>
                    <?php 
                    error_reporting(0);
                    $no=1;
                    if(isset($_GET['tanggal'])){
                    $tanggal = $_GET['tanggal'];
                    $query = "SELECT * FROM parkir JOIN kendaraan ON parkir.nomor_plat = kendaraan.nomor_plat JOIN jenis_kendaraan ON kendaraan.kode_jeniskendaraan = jenis_kendaraan.kode_jeniskendaraan WHERE tanggal_masuk = '$tanggal' ";
                    $result = mysqli_query($koneksi, $query);
                    }else {
                    $query = "SELECT * FROM parkir JOIN kendaraan ON parkir.nomor_plat = kendaraan.nomor_plat JOIN jenis_kendaraan ON kendaraan.kode_jeniskendaraan = jenis_kendaraan.kode_jeniskendaraan";
                    $result = mysqli_query($koneksi, $query);
                    }
                    
                    while ($data = mysqli_fetch_assoc($result)) {
                      $pendapatan += $data['tarif'];
                    ?>
                  <tr>
                    <td><?= $no++; ?></td>
                    <td><?= $data['nomor_plat']; ?></td>
                    <td><?= $data['jam_masuk']; ?></td>
                    <td><?= $data['tanggal_masuk']; ?></td>
                    <td><?= $data['jam_keluar']; ?></td>
                    <td><?= $data['tanggal_keluar']; ?></td>
                    <td><?= $data['tarif']; ?></td>
                    <td>
                    <button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#editlaporan<?= $no; ?>"><i class="fa fa-edit"></i></button>  
                    <a class="btn btn-danger btn-xs" href="hapus_parkir.php?nomor_parkir=<?= $data['nomor_parkir']; ?>" onclick="return confirm('Yakin Hapus ?')" ><i class="fa fa-trash"></i></a>
                    </td>
                  </tr>

                  <div class="modal fade" id="editlaporan<?= $no; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Edit Laporan Harian</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <form action="edit_laporan.php" method="post">
                        <div class="form-group">
                          <label for="nomor_plat">Nomor Plat</label>
                          <input type="hidden" name="nomor_parkir" value="<?= $data['nomor_parkir']; ?>">
                          <input type="text" name="nomor_plat" class="form-control" value="<?= $data['nomor_plat']; ?>" readonly>
                        </div>
                        <div class="form-group">
                          <label for="tanggal_masuk">Tanggal Masuk</label>
                          <input type="date" name="tanggal_masuk" class="form-control" value="<?= date('Y-m-d'); ?>">
                        </div>
                        <div class="form-group">
                          <label for="jam_masuk">Jam Masuk</label>
                          
                          <input type="time" name="jam_masuk" class="form-control" value="<?= date("H:i:s") ?>">
                        </div>
                        <div class="form-group">
                          <label for="tanggal_keluar">Tanggal Keluar</label>
                          <input type="date" name="tanggal_keluar" class="form-control" value="<?= date('Y-m-d'); ?>">
                        </div>
                        <div class="form-group">
                          <label for="jam_keluar">Jam Keluar</label>
                          <input type="time" name="jam_keluar" class="form-control" value="<?= date("H:i:s") ?>">
                        </div>
                        <div class="form-group">
                          <label for="tarif">Tarif</label>
                          <input type="hidden" name="tarif" value="<?= $data['tarif']; ?>">
                          <input type="text" name="tarif" class="form-control" value="<?= $data['tarif']; ?>" readonly>
                        </div>
                      <div class="form-group">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      <button type="submit" class="btn btn-primary float-right">Save changes</button>
                      </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>

                  <?php }  ?>

                  </tbody>
                  <tfoot>
                  <tr>
                   <th>#</th>
                    <th>Nomor Plat</th>
                    <th>Jam Masuk</th>
                    <th>Tanggal Masuk</th>
                    <th>Jam Keluar</th>
                    <th>Tanggal Keluar</th>
                    <th>Tarif</th>
                    <th width="7%">Action</th>
                  </tr>
                  </tfoot>
                 
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
  <?php include ('footer.php'); ?>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<?php include ('js.php'); ?>
</body>
</html>
