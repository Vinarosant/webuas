<?php
$page = "Index";
include "koneksi.php";
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title><?= $page ?> | Sistem Informasi Parkir</title>

  <?php include ('css.php'); ?>
<body class="hold-transition sidebar-mini">
<div class="wrapper">

  <?php include('navbar.php'); ?>

  <?php include('sidebar.php'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Sistem Informasi Parkir</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Parkir</a></li>
              <li class="breadcrumb-item active">Parkir Masuk dan Keluar</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
              <?php             
                $jumlahmasuk=mysqli_num_rows(mysqli_query($koneksi,
                "SELECT * FROM parkir WHERE jam_keluar IS NULL AND tanggal_keluar IS NULL"));
                ?>
                <h3><?= $jumlahmasuk; ?></h3>

                <p>Kendaraan Masuk</p>
              </div>
              <div class="icon">
                <i class="fa fa-plus"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
                <?php
                $jumlahkeluar=mysqli_num_rows(mysqli_query($koneksi,
                "SELECT * FROM parkir WHERE tanggal_keluar IS NOT NULL AND jam_keluar IS NOT NULL "));
                ?>
                <h3><?= $jumlahkeluar; ?></h3>

                <p>Kendaraan Keluar</p>
              </div>
              <div class="icon">
                <i class="fa fa-minus"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>

          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
              <?php
                $totalparkir=mysqli_num_rows(mysqli_query($koneksi,
                "SELECT * FROM parkir WHERE jam_masuk IS NOT NULL AND tanggal_masuk IS NOT NULL"));
                ?>
                <h3><?= $totalparkir; ?></h3>

                <p>Total Kendaraan</p>
              </div>
              <div class="icon">
                <i class="fa fa-car"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
            
          </div>
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-primary">
              <div class="inner">
                <h3><?= 100-$jumlahmasuk; ?></h3>

                <p>Sisa Parkir</p>
              </div>
              <div class="icon">
                <i class="fa fa-parking"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
        </div>
        <!-- /.row -->
        <!-- Main row -->
        <div class="row">
        </div>
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
  <?php include ('footer.php'); ?>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<?php include ('js.php'); ?>
</body>
</html>
